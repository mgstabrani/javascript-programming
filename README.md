# javascript-programming
> Javascript codes to solve Dicoding quizzes.
- [Content](#Content)
- [Author](#Author)
- [Reference](#Reference)

## Content
- [Variable and Data Type](https://github.com/mgstabrani/javascript-programming/tree/main/variable-and-data-type)
- [Logic Operator and If Else](https://github.com/mgstabrani/javascript-programming/tree/main/logic-operator-and-if-else)
- [Object](https://github.com/mgstabrani/javascript-programming/tree/main/object)
- [Array](https://github.com/mgstabrani/javascript-programming/tree/main/array)
- [Map](https://github.com/mgstabrani/javascript-programming/tree/main/map)
- [Function](https://github.com/mgstabrani/javascript-programming/tree/main/function)
- [OOP](https://github.com/mgstabrani/javascript-programming/tree/main/oop)
- [Functional Programming](https://github.com/mgstabrani/javascript-programming/tree/main/functional-programming)
- [Module](https://github.com/mgstabrani/javascript-programming/tree/main/module)
- [Error Handling](https://github.com/mgstabrani/javascript-programming/tree/main/error-handling)
- [Concurrency](https://github.com/mgstabrani/javascript-programming/tree/main/concurrency)

## Author
[Mgs. Tabrani](https://github.com/mgstabrani)

## Reference
[Dicoding](https://www.dicoding.com/)
